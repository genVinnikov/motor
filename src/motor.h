#ifndef MOTORMAINS
#define MOTORMAINS

#include "globaltimer.h"
#include "glob.h"
#include "assets.h"
#include "parser.h"
#include "init.h"
#include "input.h"
#include "window.h"
#include "onrun.h"
#include "onload.h"
#include "player.h"
#include "entity.h"

#endif
