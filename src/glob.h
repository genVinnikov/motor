#ifndef GLOB
#define GLOB

#include "mains.h"

// Global SDL2 error print
void SDLErrorPrint()
{
    std::cout << SDL_GetError() << "\n";
}

// Global SDL2_image error print
void IMGErrorPrint()
{
    std::cout << IMG_GetError() << "\n";
}

// Writes stdout to log.txt in the binary's root directory.
// Appends to log.txt if exists.
void WriteToLog()
{
    std::freopen("log.txt", "a", stdout);
}

#endif
